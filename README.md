**Quellcode**

Zu finden im helloworld-Ordner. 

Kopie von docker-02 - einziger Unterschied ist die grosse datei im Dockerfile-Verzeichnis.

Beispiel dient dazu zu zeigen, dass saemtliche Dateien und Ordner (auch Unterordner) aus dem Verzeichnis, in dem das Dockerfile liegt, an Docker geschickt werden bevor das Image gebaut wird.

**Docker**

Zu finden im docker-Ordner.

**Befehle**

Image bauen: `docker build -t dockerdemo/docker03:latest .` (im docker-Ordner, dauert aufgrund der grossen Datei recht lange)

Container starten: `docker run --rm dockerdemo/docker03` --> Wir sind im Container - nach Ausgabe von "Hello World!" ist der Container weg.

ODER

Container starten (Alternative): `docker run --rm -d dockerdemo/docker03` --> Container laeuft im Hintergrund und ist trotzdem direkt wieder weg.